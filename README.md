# Minio as S3 Bucket
This repo show how to configure a docker compose to run minio and interact with it using python and boto3 like aws bucket.

* Run it
```
$ docker-compose up
...

# MINIO GUI
# open localhost:9001
# login with access-key, secret-key
```
* Run on vscode

```
# VSCODE WEB
# open localhost:8080
# run in a terminal
$ pip install boto3
$ python /code/maindocker.py
The bucket does not exist
$ python /code/maindocker.py
The bucket exists
```

* Local run
```
$ python main.py
The bucket does not exist
$ python main.py
The bucket exists
```

# References
* docker, docker compose
* python
* minio
* boto3