import boto3

# s3 = boto3.resource('s3', 
#   endpoint_url='http://localhost:9000', 
#   config=boto3.session.Config(signature_version='s3v4')
# )

# bucket = s3.create_bucket(Bucket='my-bucket-name')


s3 = boto3.resource('s3', 
    endpoint_url='http://localhost:9000',
    aws_access_key_id='access-key',
    aws_secret_access_key='secret-key',
    region_name = 'eu-central-1',
    # aws_session_token=None,
    # config=boto3.session.Config(signature_version='s3v4'),
    # verify=False
)
 
bucket = s3.Bucket('my-bucket-name')

if bucket.creation_date:
   print("The bucket exists")
else:
   print("The bucket does not exist")
   bucket = s3.create_bucket(Bucket='my-bucket-name')
